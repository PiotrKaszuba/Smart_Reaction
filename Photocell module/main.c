#include "stm32f4xx_exti.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "misc.h"
#include "nrf24_hal.h"
#include "nrf24.h"
#include "stm32f4xx_syscfg.h"

volatile uint8_t pBuf[1] = {0};
volatile uint8_t pBuf2[1] = {1};
volatile uint8_t length = 1;
uint8_t i=0;

//MAIN IRQ
void EXTI15_10_IRQHandler(void)
{
         	if(EXTI_GetITStatus(nRF24_EXTI) != RESET)
         	{

         		while(GPIO_ReadInputDataBit(nRF24_IRQ_PORT, nRF24_IRQ_PIN))
         			if(GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_15))
         				nRF24_TransmitPacket(pBuf,length,1);

         			else
         				nRF24_TransmitPacket(pBuf2,length,1);


         		EXTI_ClearITPendingBit(nRF24_EXTI);
         	}
}

//Initialize
int main(void)
{
	SystemInit();

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	GPIO_InitTypeDef GP;

	GP.GPIO_Pin = GPIO_Pin_15 ;
	GP.GPIO_Mode = GPIO_Mode_OUT;
	GP.GPIO_OType = GPIO_OType_PP;
	GP.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_Init(GPIOD, &GP);

	GP.GPIO_Pin = GPIO_Pin_15;
	GP.GPIO_Mode = GPIO_Mode_IN;
	GP.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOC, &GP);
	int j=0;
	for(;j<500000;j++);

	nRF24_SPI_Init();
	if (!nRF24_Check()) GPIO_SetBits(GPIOD,GPIO_Pin_15);

	nRF24_Init_SimpleBase();
	nRF24_Init_SimpleTx();

	nRF24_IRQ_Init();



    while(1)
    {

    }
}
