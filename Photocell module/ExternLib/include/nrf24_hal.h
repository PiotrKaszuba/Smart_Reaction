#ifndef __NRF24_HAL_H
#define __NRF24_HAL_H


//Need to implement EXTI IRQ yourself


// Peripheral libraries
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_spi.h>
#include "misc.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"

// SPI port peripheral
#define nRF24_SPI_PORT             SPI2

// nRF24 SPI peripherals
#define nRF24_SPI_PERIPHERALS	   (RCC_APB1ENR_SPI2EN)

// SPI lines

#define nRF24_SPI_LINES_PORT        GPIOB
#define nRF24_SPI_MISO_PIN			GPIO_Pin_14
#define nRF24_SPI_MOSI_PIN			GPIO_Pin_15
#define nRF24_SPI_CLK_PIN			GPIO_Pin_13
#define nRF24_SPI_CS_PIN           GPIO_Pin_12

#define nRF24_CLK_PINSOURCE	GPIO_PinSource13
#define nRF24_MISO_PINSOURCE	GPIO_PinSource14
#define nRF24_MOSI_PINSOURCE	GPIO_PinSource15

#define nRF24_GPIO_AF_SPI	GPIO_AF_SPI2

#define nRF24_CSN_L()              GPIO_ResetBits(nRF24_SPI_LINES_PORT, nRF24_SPI_CS_PIN)
#define nRF24_CSN_H()              GPIO_SetBits(nRF24_SPI_LINES_PORT, nRF24_SPI_CS_PIN)


// nRF24 GPIO peripherals
#define nRF24_GPIO_PERIPHERALS     (RCC_AHB1ENR_GPIOBEN)
#define nRF24_IRQ_PERIPHERALS     (RCC_AHB1ENR_GPIOBEN)
// CE (chip enable) pin (PB11)
#define nRF24_CE_PORT              GPIOB
#define nRF24_CE_PIN               GPIO_Pin_11
#define nRF24_CE_L()               GPIO_ResetBits(nRF24_CE_PORT, nRF24_CE_PIN)
#define nRF24_CE_H()               GPIO_SetBits(nRF24_CE_PORT, nRF24_CE_PIN)



// IRQ pin (PB10)
#define nRF24_IRQ_PORT             GPIOB
#define nRF24_IRQ_PIN              GPIO_Pin_10

#define nRF24_NVIC_GROUP		NVIC_PriorityGroup_1
#define nRF24_EXTI_PERIPHERALS	(RCC_APB2ENR_SYSCFGEN)
#define nRF24_NVIC_CHANNEL		EXTI15_10_IRQn
#define nRF24_EXTI				EXTI_Line10
#define nRF24_NVIC_SUBPRIORITY	0x01
#define nRF24_NVIC_PREEMPTION	0x00
#define nRF24_EXTI_IRQ_PINSOURCE	EXTI_PinSource10
#define nRF24_EXTI_IRQ_PORTSOURCE EXTI_PortSourceGPIOB
// Function prototypes

//TODO
void EXTI15_10_IRQHandler(void);

void nRF24_SPI_Init(void);
void nRF24_IRQ_Init(void);
uint8_t nRF24_LL_RW(uint8_t data);

#endif // __NRF24_HAL_H
