#include "nrf24_hal.h"


// Configure the GPIO lines of the nRF24L01 transceiver
// note: IRQ pin must be configured separately
void nRF24_SPI_Init(void) {
    GPIO_InitTypeDef PORT;
    SPI_InitTypeDef SPI;
    // Enable the nRF24L01 GPIO peripherals
    RCC->AHB1ENR |= nRF24_GPIO_PERIPHERALS;
    RCC->APB1ENR |= nRF24_SPI_PERIPHERALS;

    // Configure SPI lines
	PORT.GPIO_Mode = GPIO_Mode_AF;
	PORT.GPIO_OType = GPIO_OType_PP;
	PORT.GPIO_Speed = GPIO_Speed_50MHz;
	PORT.GPIO_Pin = nRF24_SPI_CLK_PIN | nRF24_SPI_MISO_PIN | nRF24_SPI_MOSI_PIN;
	GPIO_Init(nRF24_SPI_LINES_PORT, &PORT);

	// SCK
	GPIO_PinAFConfig(nRF24_SPI_LINES_PORT, nRF24_CLK_PINSOURCE, nRF24_GPIO_AF_SPI);
	// MISO
	GPIO_PinAFConfig(nRF24_SPI_LINES_PORT, nRF24_MISO_PINSOURCE, nRF24_GPIO_AF_SPI);
	// MOSI
	GPIO_PinAFConfig(nRF24_SPI_LINES_PORT, nRF24_MOSI_PINSOURCE, nRF24_GPIO_AF_SPI);

	PORT.GPIO_Mode = GPIO_Mode_OUT;
	PORT.GPIO_Pin = nRF24_SPI_CS_PIN;
	PORT.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(nRF24_SPI_LINES_PORT, &PORT);
	nRF24_CSN_H();



	SPI.SPI_Mode = SPI_Mode_Master;
	SPI.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_2;
	SPI.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI.SPI_CPOL = SPI_CPOL_Low;
	SPI.SPI_CPHA = SPI_CPHA_1Edge;
	SPI.SPI_CRCPolynomial = 7;
	SPI.SPI_DataSize = SPI_DataSize_8b;
	SPI.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI.SPI_NSS = SPI_NSS_Soft;
	SPI_Init(nRF24_SPI_PORT, &SPI);
	SPI_NSSInternalSoftwareConfig(nRF24_SPI_PORT, SPI_NSSInternalSoft_Set);
	SPI_Cmd(nRF24_SPI_PORT, ENABLE);



	// Configure CE pin
	PORT.GPIO_Pin = nRF24_CE_PIN;
	GPIO_Init(nRF24_CE_PORT, &PORT);
	nRF24_CE_L();
}


void nRF24_SPI_Disable()
{
	SPI_Cmd(nRF24_SPI_PORT, DISABLE);
}

void nRF24_SPI_Enable()
{
	SPI_Cmd(nRF24_SPI_PORT, ENABLE);
}

/*
void EXTI15_10_IRQHandler(void)
{
         	if(EXTI_GetITStatus(nRF24_EXTI) != RESET)
         	{


         		EXTI_ClearITPendingBit(nRF24_EXTI);
         	}
}
*/


void nRF24_IRQ_Init(void){

	  RCC->AHB1ENR |= nRF24_IRQ_PERIPHERALS;

	  NVIC_PriorityGroupConfig(nRF24_NVIC_GROUP);

	  RCC->APB2ENR |=nRF24_EXTI_PERIPHERALS;




	  GPIO_InitTypeDef PORT;
	  PORT.GPIO_Mode = GPIO_Mode_IN;
	  //PORT.GPIO_OType = GPIO_OType_PP;
	  PORT.GPIO_Speed = GPIO_Speed_100MHz;
	  PORT.GPIO_PuPd = GPIO_PuPd_DOWN;
	  PORT.GPIO_Pin = nRF24_IRQ_PIN;
	  GPIO_Init(nRF24_IRQ_PORT, &PORT);

	  NVIC_InitTypeDef NVIC_InitStructure;

	  NVIC_InitStructure.NVIC_IRQChannel = nRF24_NVIC_CHANNEL;
	  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = nRF24_NVIC_PREEMPTION;
	  NVIC_InitStructure.NVIC_IRQChannelSubPriority = nRF24_NVIC_SUBPRIORITY;
	  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	  NVIC_Init(&NVIC_InitStructure);


	  EXTI_InitTypeDef EXTI_InitStructure;
	  EXTI_InitStructure.EXTI_Line = nRF24_EXTI;
	  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	  EXTI_Init(&EXTI_InitStructure);

	  SYSCFG_EXTILineConfig(nRF24_EXTI_IRQ_PORTSOURCE, nRF24_EXTI_IRQ_PINSOURCE);
	  EXTI_ClearITPendingBit(nRF24_EXTI);


}



// Low level SPI transmit/receive function (hardware depended)
// input:
//   data - value to transmit via SPI
// return: value received from SPI
uint8_t nRF24_LL_RW(uint8_t data) {
	 // Wait until TX buffer is empty
	while (SPI_I2S_GetFlagStatus(nRF24_SPI_PORT, SPI_I2S_FLAG_TXE) == RESET);
	// Send byte to SPI (TXE cleared)
	SPI_I2S_SendData(nRF24_SPI_PORT, data);
	// Wait while receive buffer is empty
	while (SPI_I2S_GetFlagStatus(nRF24_SPI_PORT, SPI_I2S_FLAG_RXNE) == RESET);

	// Return received byte
	 uint8_t temp= SPI_I2S_ReceiveData(nRF24_SPI_PORT);
	 return temp;
}


