#pragma once
#include "stm32f4xx_tim.h"
#include "misc.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"
#include "stm32f4xx_usart.h"
#include "tm_stm32f4_lis302dl_lis3dsh.h"
#include "audio.h"
#include "stm32f4xx_dma.h"
#include "stm32f4xx_spi.h"
#include <stdlib.h>
#include "nrf24.h"
#include "nrf24_hal.h"
#include "codec.h"






// zmienne globalne
extern volatile DMA_InitTypeDef DMA;
//dolny prog
extern volatile int low;
//gorny prog
extern volatile int high;
//aktualne ustawienie akcelerometru
extern volatile int tab[500];
//struct for accelerometer data
extern volatile TM_LIS302DL_LIS3DSH_t Axes_Data;


//audio counter
extern volatile unsigned int global;
//time counter in ms
extern volatile unsigned int ms;
extern volatile int time;
extern volatile int false_start;
// loop var
extern int i;
extern int suma ;
// send data  var
extern volatile unsigned char  c[4];
// get radio signal buffer
extern volatile uint8_t radio_buffer[1];
extern volatile unsigned char mid_gates_char_signals[1];
//application register
// bits power order
// 0 - wait for reaction (if runner started)
// 1 - play audio
// 2 - send data
// 3 - false start hanging
// 4 - wait for start sig (system start)
// 5 - wait for radio sig (runner gate cross)
// 6 - measure accelerometer (on initialize)
// 7 - wait for initialize sig
extern volatile unsigned char reg;

//0 - next time send reaction data
//1 - next time send midgate data
//2 - next time send finish data
//3 - only reaction state
//4 - finish state
//5 - mid gate state
//6 - check mid gate state
//7 - check finish line state
extern volatile unsigned char reg2;

void init_globals();

