
#include "globals.h"

// zmienne globalne
volatile DMA_InitTypeDef DMA;
//dolny prog
 volatile int low;
//gorny prog
 volatile int high;
//aktualne ustawienie akcelerometru
 volatile int tab[500];
//struct for accelerometer data
 volatile TM_LIS302DL_LIS3DSH_t Axes_Data;


//audio counter
 volatile unsigned int global=0;
//time counter in ms
 volatile unsigned int ms =-1;
 volatile int time;
 volatile int false_start=0;
// loop var
 int i=0;
 int suma =0;
// send data  var
 volatile unsigned char  c[4] = { 'x', 0, 0, 0 };
// get radio signal buffer
 volatile uint8_t radio_buffer[1] = { 'x' };
 volatile unsigned char mid_gates_char_signals[1] = { 'a'};
//application register
// bits power order
// 0 - wait for reaction (if runner started)
// 1 - play audio
// 2 - send data
// 3 - false start hanging
// 4 - wait for start sig (system start)
// 5 - wait for finish sig (runner finish) <<todo>>
// 6 - measure accelerometer (on initialize)
// 7 - wait for initialize sig
 volatile unsigned char reg= 0x01 << 7;

 //0 - next time send reaction data
 //1 - next time send midgate data
 //2 - next time send finish data
 //3 - only reaction state
 //4 - finish state
 //5 - mid gate state
 //6 - check mid gate state
 //7 - check finish line state
 volatile unsigned char reg2 = 0;


void init_globals(){
global = 0;
ms = -1;
false_start = 0;
i = 0;
suma = 0;
reg = 0x01 << 7;
reg2 = 0;
TIM3->CNT = 0;

}
