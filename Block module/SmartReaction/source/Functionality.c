
#include "Functionality.h"


void measure_accelerometer_position()
{

		//count, quit measuring, change state
		if (ms >= 500)
		{
			for (i = 0; i<500; i++)
			{
				suma += tab[i] / 100;
			}
			suma /= 5;
			low = suma - 1000;
			high = suma + 1000;
			suma = 0;
			i = 0;
			//measure = 0, wait for start = 1
			reg = reg & ~(0x01 << 6) | (0x01 << 4);
		}
		//measuring actions, first should be ms =0;
		else
		{
			tab[ms] = Axes_Data.Z;
		}



}




void check_for_reaction()
{
	//redukujemy czas od poruszenia ostatniego na rzecz rozpatrywania falstartu
	if (false_start) false_start--;
	//jesli przekroczenie wartosci przyspieszen (nadana sila)
	if (Axes_Data.Z > high || Axes_Data.Z < low)
	{
		//jesli po sygnale startu
		if (reg & 0x01)
		{
			//zapisujemy w ktorej milisekundzie start
			time = ms;
			// reaction measure =0, send data =1
			reg = reg & ~0x01 | (0x01 << 2);
		}
		//jesli przed sygnalem startu
		else
		{
			//zakres wykrywania falstartu do -200ms, jesli spadnie do 0 to od nowa od zakresu
			if(!false_start) false_start = 200;
		}
	}
}

void set_send_code(){

	// obliczanie kodu zwrotu za pomoca stanu
	c[1] = 0;

	// jesli nastepnie ma byc wyslana reakcja (tylko w przypadku startow nie sprawdzen)
	if (reg2 & (0x01))
		c[1] +=3;

	// jesli nastepnie ma byc wyslany midgate, ale nie jest to stan sprawdzenia midgate
	if ((reg2 & (0x01 << 1)) && !(reg2 & (0x01 << 6)))
		c[1] +=4;

	// jesli inny stan od tylko reakcji
	if (!(reg2 & (0x01 << 3)))
		c[1]+=1;

	// jesli stan sprawdzenia ktorys
	if((reg2 & (0x01 << 6)) || (reg2 & (0x01 << 7)))
		c[1]+=1;


	// jesli false start to kod F
	if (reg & (0x01 << 3))
		c[1] = 'F';



}

void reaction_time_code(){

	// 0.001secs
	c[2] = time % 100;
	//0.1 secs
	c[3] = time / 100;
}

void run_time_code(){

	// 0.01 secs
	c[2] = ((time % 1000) /10);
	//1 secs
	c[3] = time / 1000;
}

void restart(){

	//przywracamy zmienne do stanu poczatkowego
	init_globals();
	//przywroc dzialanie timera
	TIM_Cmd(TIM3, ENABLE);
	//czekamy na kolejny sygnal startu
	//init = 0, wait start sig =1
	reg = reg & ~(0x01 << 7) | (0x01 << 6);
}

void what_to_send(){

	//wylaczmy wysylanie danych (po wyslaniu jestesmy)
	//send data = 0
	reg = reg & ~(0x01 << 2);
	//jesli stan tylko reakcja lub jesli wyslalismy finish(pokryte sprawdzenie finish)
	//lub jesli sprawdzalismy midgate to nic nie wysylamy potem
	if ((reg2 & (0x01 << 3)) || (reg2 & (0x01 << 2)) || (reg2 & (0x01 << 6))){

		return;
	}

	//jesli tutaj doszlo to znaczy ze stan to nie tylko reakcja
	//ani sprawdzenie
	//skoro juz wyslalismy raz dane to znaczy ze wyslalismy reakcje
	//wlaczmy nasluchiwanie radia
	//wait for radio sig=1
	reg = reg | (0x01 <<5);
	//jesli stan finish
	if (reg2 & (0x01 <<4))
		//jesli wysylalismy reakcje to nastepnie wyslemy finish
		if(reg2 & 0x01){
			//next time send reaction=0, next time send finish =1
			reg2= reg2 & ~(0x01) | (0x01 << 2);
			return;
		}


	//jesli stan midgate
	if (reg2 & (0x01 <<5)){
		//jesli wysylalismy reakcje to nastepnie wyslemy midgate
		if(reg2 & 0x01){
			//next time send reaction=0, next time send midgate =1
			reg2= reg2 & ~(0x01) | (0x01 << 1);
			return;
		}

		//jesli wysylalismy midgate to nastepnie wyslemy finish
		if(reg2 & (0x01 << 1)){
			//next time send midgate=0, next time send finish =1
			reg2= reg2 & ~(0x01 << 1) | (0x01 << 2);
			return;
		}

	}
}

void send_data()
{

	set_send_code();

	//jesli nastepnie ma byc wyslana reakcja to kodujemy czas z dokladnoscia do 0.001
	if (reg2 & 0x01)
		reaction_time_code();
	else
		//jesli czas z biegu to dokladnosc do 0.01
		run_time_code();

	//wyslanie wszystkich bajtow
	for (i = 0; i < 4; i++)
	{
		//czekaj na opróżnienie bufora wyjściowego
		while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
		// wyslanie danych
		USART_SendData(USART2, c[i]);
		// czekaj az dane zostana wyslane
		while (USART_GetFlagStatus(USART2, USART_FLAG_TC) == RESET);
	}

	//ustalenie co ma byc wyslane nastepne lub reset systemu jesli wszystko
	what_to_send();
}

void radio_sig(){
	//jesli cos zostalo odebrane
	if (nRF24_ReceivePacket(radio_buffer,1))
		//jesli nastepnie chcemy wyslac finish to sprawdzamy czy odebrano kod finish
		// lub jesli chcemy wyslac midgate to sprawdzamy czy odebrano midgate
		if((reg2 & (0x01 <<2) && radio_buffer[0] == 0) || (reg2 & (0x01 <<1) && radio_buffer[0] == 1))
		{
			// zapisz czas do wyslania
			time=ms;
			// wait radio =0, send data = 1
			reg = reg & ~(0x01 << 5) | (0x01 << 2);
		}

	// jesli sprawdzenie ktores
	if((reg2 & (0x01 << 6)) || (reg2 & (0x01 << 7)))
		//jesli czas powyzej 10s przerwij i wyslij informacje
		if(ms>10000)
		{
			time=ms;
			reg = reg & ~(0x01 << 5) | (0x01 << 2);
		}


}

void start_base(){

	while(nRF24_ReceivePacket(radio_buffer,1));
	// wait start sign =0, audio=1
	reg = reg & ~(0x01 << 4) | (0x01 << 1);

	// next time send reaction data=1
	reg2 = reg2 | 0x01;

	// jesli sygnal startu po poruszeniu
	if (false_start) {
		//false start hanging = 1, send data=1
		reg = reg | (0x01 << 3) | (0x01 << 2);
		//zmieniamy na tylko reakcja kiedy falstart
		reg2 = reg2 & ~((0x01 << 4) | (0x01 << 5));
		reg2 = reg2 | (0x01 << 3);
		//zapisujemy czas falstartu do wyslania
		time = false_start;
	}
	else {
		// w przeciwnym wypadku mozemy oczekiwac prawdziwej reakcji
		// wait for reaction =1
		reg = reg | 0x01;

	}

	// ms bedzie rosnac i na koniec bedzie zapisane w time
	ms = 0;

	// zeby dzwiek zaczal sie zaraz potem (/49)
	TIM4->CNT = 48;
	TIM_Cmd(TIM4, ENABLE);
}


void only_reaction_case(){

	//only reaction state = 1
	reg2 = reg2 | (0x01 << 3);
	start_base();
}

void sprint_case(){

	//finish state = 1
	reg2 = reg2 | (0x01 << 4);
	start_base();
}

void midgate_case(){

	//midgate state = 1
	reg2 = reg2 | (0x01 << 5);
	start_base();
}

void init_case(){

	init_globals();
	enable_all();
	//init = 0, measure accelerometer position = 1
	reg = reg & ~(0x01 << 7) | (0x01 << 6);


}

void check_finish_case(){


	// wait start sign = 0, radio sig = 1
	reg = reg & ~(0x01 << 4) | (0x01 << 5);
	// check finish state = 1, next time send finish data = 1
	reg2 = reg2 | (0x01 << 7) | (0x01 << 2);
	ms = 0;
	while(nRF24_ReceivePacket(radio_buffer,1));

}

void check_midgate_case(){


	// wait start sign = 0, radio sig = 1
	reg = reg & ~(0x01 << 4) | (0x01 << 5);
	// check midgate state = 1, next time send midgate data = 1
	reg2 = reg2 | (0x01 << 6) | (0x01 << 1);
	ms = 0;
	while(nRF24_ReceivePacket(radio_buffer,1));
}

void enable_all(){

	nRF24_SPI_Enable();

	SPI_I2S_DMACmd(CODEC_I2S, SPI_I2S_DMAReq_Tx, ENABLE);

	I2S_Cmd(CODEC_I2S, ENABLE);

	TIM_Cmd(TIM3, ENABLE);
}


void audio(){
	//graj nastepny bajt

	//co 2 dzwiek brzmi dobrze a rzadziej przerwanie
	global+=2;
	//rekonfiguracja DMA na nowy bajt cyklicznie przesylany
	DMA_Cmd(DMA1_Stream7, DISABLE);
	//wpisz do rejestru adres bajtu do przesylania
	//jesli falstart to inny dzwiek
	if(reg & (0x01 << 3))
		DMA1_Stream7->M0AR = (uint32_t)&falseAudiox+global;
	else
		DMA1_Stream7->M0AR = (uint32_t)&rawAudiox+global;
	DMA_Cmd(DMA1_Stream7, ENABLE);



	//sprawdz czy dzwiek sie nie skonczyl (jesli falstart to dzwiek krotszy)
	if (global > 54000 - 500*(reg &(0x01<<3))) {
			//audio=0
			reg = reg & ~(0x01 <<1);
			//wylacz przesylanie bajtu, zresetuj adres i wylacz zegar przerwania
			DMA_Cmd(DMA1_Stream7, DISABLE);
			global = 0;
			TIM_Cmd(TIM4, DISABLE);
			//odblokowanie na wypadek zatrzymania w celu pozwolenia wybrzmienia dzwieku
			TIM_Cmd(TIM3, ENABLE);
		}
}


void check_start_control_byte(){

	// odebrany bajt znajduje sie w rejestrze USART2->DR
	switch(USART2->DR){
		case 'r':
			only_reaction_case();
			break;
		case 's':
			sprint_case();
			break;
		case 'a':
			midgate_case();
			break;
		case 'c':
			check_finish_case();
			break;
		case 'd':
			check_midgate_case();
			break;
	}
}

void check_init_control_byte(){

	if (USART2->DR == 'i')
		init_case();
}
