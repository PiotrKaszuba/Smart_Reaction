#include "init_periph.h"
void init_peripherals()
{
	SystemInit();
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

	//RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);


	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	 RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);

	//audio codec
	codec_init();
	codec_ctrl_init();


	GPIO_InitTypeDef GP;

	GP.GPIO_Pin = GPIO_Pin_15 | GPIO_Pin_14;
	GP.GPIO_Mode = GPIO_Mode_OUT;
	GP.GPIO_OType = GPIO_OType_PP;
	GP.GPIO_Speed = GPIO_Speed_50MHz;

	GPIO_Init(GPIOD, &GP);



	// 9999 * 8399 = 1s
	TIM_TimeBaseInitTypeDef TM;
	TM.TIM_Period = 999;
	TM.TIM_Prescaler =83;
	TM.TIM_ClockDivision = TIM_CKD_DIV1;
	TM.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM3, &TM);



	TM.TIM_Period = 49;
	TM.TIM_Prescaler = 99;
	TM.TIM_ClockDivision = TIM_CKD_DIV1;
	TM.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM4, &TM);


	NVIC_InitTypeDef NVICinit;


	NVICinit.NVIC_IRQChannel = TIM3_IRQn;
	NVICinit.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVICinit.NVIC_IRQChannelSubPriority = 0x00;
	NVICinit.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVICinit);
	// wyczyszczenie przerwania od timera 4 (wyst�pi�o przy konfiguracji timera)
	TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
	// zezwolenie na przerwania od przepe�nienia dla timera 4
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

	NVICinit.NVIC_IRQChannel = TIM4_IRQn;
	NVICinit.NVIC_IRQChannelPreemptionPriority = 0x01;
	NVICinit.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_Init(&NVICinit);
	TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);




	//USART
	//GPIOA 2 to TX 3 to RX
	// konfiguracja linii Rx i Tx

	GP.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GP.GPIO_Mode = GPIO_Mode_AF;
	GP.GPIO_OType = GPIO_OType_PP;
	GP.GPIO_PuPd = GPIO_PuPd_UP;
	GP.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GP);
	// ustawienie funkcji alternatywnej dla pin�w (USART)
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);


	USART_InitTypeDef USART_InitStructure;
	// predkosc transmisji (mozliwe standardowe opcje: 9600, 19200, 38400, 57600, 115200, ...)
	USART_InitStructure.USART_BaudRate = 9600;
	// d�ugo�� s�owa (USART_WordLength_8b lub USART_WordLength_9b)
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	// liczba bit�w stopu (USART_StopBits_1, USART_StopBits_0_5, USART_StopBits_2, USART_StopBits_1_5)
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	// sprawdzanie parzysto�ci (USART_Parity_No, USART_Parity_Even, USART_Parity_Odd)
	USART_InitStructure.USART_Parity = USART_Parity_No;
	// sprz�towa kontrola przep�ywu (USART_HardwareFlowControl_None, USART_HardwareFlowControl_RTS, USART_HardwareFlowControl_CTS, USART_HardwareFlowControl_RTS_CTS)
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	// tryb nadawania/odbierania (USART_Mode_Rx, USART_Mode_Rx )
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART2, &USART_InitStructure);



	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	NVICinit.NVIC_IRQChannel = USART2_IRQn;
	NVICinit.NVIC_IRQChannelPreemptionPriority = 0x01;
	NVICinit.NVIC_IRQChannelSubPriority = 0x01;
	NVIC_Init(&NVICinit);
	NVIC_EnableIRQ(USART2_IRQn);


	//akcelerometr
	TM_LIS302DL_LIS3DSH_Device_t IMU_Type;
	IMU_Type = TM_LIS302DL_LIS3DSH_Detect();
	TM_LIS302DL_LIS3DSH_Init(TM_LIS3DSH_Sensitivity_2G, TM_LIS3DSH_Filter_800Hz);


	//DMA
	DMA.DMA_Channel = DMA_Channel_0;
	DMA.DMA_DIR=DMA_DIR_MemoryToPeripheral;
	DMA.DMA_Mode = DMA_Mode_Circular;
	DMA.DMA_Memory0BaseAddr =  (uint32_t)&rawAudiox;
	DMA.DMA_PeripheralBaseAddr = (uint32_t) &SPI3->DR;
	DMA.DMA_Priority = DMA_Priority_Medium;
	DMA.DMA_BufferSize = (uint32_t) 1;
	DMA.DMA_MemoryInc = DMA_MemoryInc_Disable;
	DMA.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA.DMA_FIFOThreshold=DMA_FIFOThreshold_Full;
	DMA_Init(DMA1_Stream7, &DMA);
	DMA1_Stream7->CR = DMA1_Stream7->CR | DMA_SxCR_TCIE;

	//Radio
	nRF24_SPI_Init();
	if (!nRF24_Check()) GPIO_SetBits(GPIOD,GPIO_Pin_15);
	nRF24_Init_SimpleBase();
	nRF24_Init_SimpleRx();
	nRF24_SPI_Disable();


}
