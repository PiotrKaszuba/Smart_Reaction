

#include "IRQs.h"



//every ms
void TIM3_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
		{
			//kolejna milisekunda jak timer
			ms++;
			//pobieramy wartosc przyspieszen
			TM_LIS302DL_LIS3DSH_ReadAxes(&Axes_Data);

//----------------------------------------------------------------------------------------
			//jesli inicjalizujemy to ustalamy orientacje akcelerometru
			if (reg & (0x01 << 6))
				measure_accelerometer_position();

				//w przeciwnym wypadku rozpatrujemy akcje na ustawionym akcelerometrze
				else
					//reg 0 bajt obsluzony tutaj
					check_for_reaction();


			//jesli mamy wynik to wysylamy dane
			if (reg & (0x01 << 2))
				//reg2 obsluzony tutaj
				send_data();


			//jesli czekamy na radio (juz na pewno po reakcji)
			if (reg & (0x01 << 5))
				radio_sig();
			//jesli (niezaleznie od falstartu) nic sie nie dzieje juz to restart
			if (!(reg & ~(0x01 << 3)))
						restart();


//-----------------------------------------------------------------------------------------


			TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
		}
}



//audio play
void TIM4_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)
	{
		//jesli audio to graj
		if (reg & (0x01 << 1))
			audio();


		TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
	}
}

//signal Bluetooth
void USART2_IRQHandler(void)
{
	// sprawdzenie flagi zwiazanej z odebraniem danych przez USART
	if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{

		// jesli oczekiwanie na sygnal startu
		if(reg & (0x01 << 4))
			//reg bajt 3 obsluzony tutaj (w kazdej z komend do startu a nie sprawdzajacej)
			check_start_control_byte();


		// jesli oczekiwanie na sygnal poczatkowy
		if(reg & (0x01 << 7))
			check_init_control_byte();


		USART_ClearITPendingBit(USART2, USART_IT_RXNE);
	}
}
